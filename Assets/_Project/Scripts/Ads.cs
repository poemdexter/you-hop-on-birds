﻿using Prime31.WinPhoneAdMob;
using Prime31.WinPhoneEssentials;
using UnityEngine;
using System.Collections;

public class Ads : MonoBehaviour
{
    public string iOSAdUnit, wp8AdUnit, androidAdUnit;
    public int score = 0;

#if UNITY_WP8

    public void Start()
    {
        if (PlayerPrefs.HasKey("BEST"))
        {
            score = PlayerPrefs.GetInt("BEST");
        }
        UpdateWP8Tile();
    }

    public void UpdateWP8Tile()
    {
        // first, create the tile data
        var tileData = new FlipTileData();
        tileData.backContent = "High Score: " + score;
        tileData.wideBackContent = "High Score: " + score;
        tileData.smallBackgroundImage = "Assets/Tiles/FlipCycleTileSmall.png";
        tileData.backgroundImage = "Assets/Tiles/FlipCycleTileMedium.png";
        tileData.wideBackgroundImage = "Assets/Tiles/FlipCycleTileLarge.png";
        // now update the tile
        Tiles.createOrUpdateSecondaryLiveTile("flippy-tile", tileData);
    }

    public void ShowMarketplaceReview()
    {
        Sharing.showMarketplaceReviewPage();
    }

    public void ShowBanner()
    {
        // replace with your ad unit!!!!
        WinPhoneAdMob.createBanner(wp8AdUnit, AdFormat.Banner, AdHorizontalAlignment.Center, AdVerticalAlignment.Bottom,
            true);
    }

    public void HideBanner()
    {
        WinPhoneAdMob.removeBanner();
    }

#endif

#if UNITY_ANDROID || UNITY_IPHONE

    private void Start()
    {
        Init();
    }

    public void Init()
    {
        Debug.Log("Initializing Ads");
        AdMob.init(androidAdUnit, iOSAdUnit);
    }

    public void UpdateWP8Tile() {}
    public void ShowMarketplaceReview() {}

    public void ShowBanner()
    {
        Debug.Log("Showing Banner");
        AdMob.createBanner(AdMobBanner.SmartBanner, AdMobLocation.BottomCenter);
    }

    public void HideBanner()
    {
        Debug.Log("Hiding Banner");
        AdMob.destroyBanner();
    }

#endif

}
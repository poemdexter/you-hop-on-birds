﻿using UnityEngine;
using System.Collections;

public class PlayerMovement : MonoBehaviour
{
    private float movementLeft, movementRight;
    public float horizontalSpeed;
    public float bouncePower;
    public float divePower;
    private bool goingUp, diving;
    private Animator anim;
    private GameController game;
    public ParticleSystem blood;

    private void Start()
    {
        anim = GetComponent<Animator>();
        game = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();
    }

    private void Update()
    {
        transform.rotation = Quaternion.identity;

        if (!diving && Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began)
        {
            diving = true;
            rigidbody2D.velocity = Vector2.zero;
            rigidbody2D.AddForce(-1 * (Vector2.up * divePower));
            anim.SetTrigger("DIVE");
        }

        if (!diving)
        {
            if (goingUp && rigidbody2D.velocity.y < 0)
            {
                goingUp = false;
                anim.SetTrigger("DOWN");
            }
            else if (!goingUp && rigidbody2D.velocity.y > 0)
            {
                goingUp = true;
                anim.SetTrigger("UP");
            }

            float delta = Time.deltaTime*Input.acceleration.x*horizontalSpeed;
            if (transform.position.x + delta > -3.23 && transform.position.x + delta < 3.23)
                transform.Translate(Vector2.right*delta);
        }
    }

    private void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.CompareTag("bird"))
        {
            AudioController.Play("jump");
            rigidbody2D.velocity = Vector2.zero;
            rigidbody2D.AddForce(Vector2.up*bouncePower);
            diving = false;
            goingUp = true;
            anim.SetTrigger("UP");
            game.Scored();
        }

        if (col.gameObject.CompareTag("spike"))
        {
            AudioController.Play("hurt");
            ParticleSystem particles = (ParticleSystem) Instantiate(blood, transform.position, Quaternion.identity);
            particles.Play();
            Camera.main.GetComponent<Screenshake>().Shake();
            Handheld.Vibrate();
            Destroy(gameObject);
            game.Died();
        }
    }
}
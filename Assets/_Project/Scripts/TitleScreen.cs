﻿using UnityEngine;
using System.Collections;

public class TitleScreen : MonoBehaviour
{
    public UILabel best;

    private void Start()
    {
        if (PlayerPrefs.HasKey("BEST"))
        {
            int b = PlayerPrefs.GetInt("BEST");
            best.text = "BEST: " + b;
        }    
    }

    public void LoadGame()
    {
        AudioController.Play("button");
        Application.LoadLevel("game");
    }

    public void Twitter()
    {
        Application.OpenURL("http://twitter.com/poemdextergames");
    }
}
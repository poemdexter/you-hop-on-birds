﻿using UnityEngine;
using System.Collections;

public class Screenshake : MonoBehaviour
{
    public float shakeAmount = 0.7f;
    public float decreaseFactor = 1.0f;
    private float shake;

    private bool shakeEnded = true;
    private Vector3 originalPosition;

    public void Shake()
    {
        originalPosition = transform.position;
        shake = shakeAmount;
        shakeEnded = false;
    }

    void Update()
    {
        if (shake > 0)
        {
            transform.position = originalPosition;
            Vector3 pos = Random.insideUnitSphere * shake;
            transform.position += new Vector3(pos.x, pos.y, 0);
            shake -= Time.deltaTime * decreaseFactor;
        }
        else if (!shakeEnded)
        {
            shakeEnded = true;
            transform.position = originalPosition;
            shake = 0;
        }
    }
}
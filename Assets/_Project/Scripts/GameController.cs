﻿using UnityEngine;
using System.Collections;

public class GameController : MonoBehaviour
{
    public UILabel playerScore;
    private int score, bestScore;
    public GameObject birdPrefab;
    private GameObject bird;
    public GameObject player;
    public UIPanel gameoverPanel;
    public UILabel gameoverScore, gameoverBest;
    public UIButton button;
    private float birdSpeed;
    public float minHeight = -2f;
    public float maxHeight = 1f;
    public float minSpeed, maxSpeed;
    private Ads ads;

    private void Start()
    {
        bird = (GameObject) Instantiate(birdPrefab, new Vector2(-6f, 1f), Quaternion.identity);
        if (PlayerPrefs.HasKey("BEST"))
        {
            bestScore = PlayerPrefs.GetInt("BEST");
        } 
        Setup();
        ads = GameObject.FindGameObjectWithTag("Ads").GetComponent<Ads>();
    }

    private void Update()
    {
        bird.transform.Translate(Time.deltaTime*birdSpeed*Vector2.right);
    }

    public void Setup()
    {
        bool west = Random.Range(0, 2) == 1;
        birdSpeed = Random.Range(minSpeed, maxSpeed);
        if (!west)
        {
            birdSpeed = -birdSpeed;
            bird.transform.localScale = new Vector3(-1f,1f,1f);
        }
        else
        {
            bird.transform.localScale = new Vector3(1f, 1f, 1f);
        }
        bird.transform.position = new Vector2((west) ? -5f : 5f, Random.Range(minHeight, maxHeight));
    }

    public void Died()
    {
        ads.ShowBanner();
        gameoverScore.text = "Score: " + score;
        CalculateBestScore();
        gameoverBest.text = "Best: " + bestScore;
        gameoverPanel.enabled = true;
        button.enabled = true;
    }

    private void CalculateBestScore()
    {
        if (score > bestScore)
        {
            bestScore = score;
            PlayerPrefs.SetInt("BEST", bestScore);
            PlayerPrefs.Save();
            ads.score = bestScore;
            ads.UpdateWP8Tile();
        }
    }

    public void Scored()
    {
        score++;
        playerScore.text = "" + score;
    }

    public void Reset()
    {
        ads.HideBanner();
        AudioController.Play("button");
        button.enabled = false;
        gameoverPanel.enabled = false;
        score = 0;
        playerScore.text = "" + score;
        Setup();
        Instantiate(player);
    }
}
﻿using UnityEngine;
using System.Collections;

public class KillParticleEffect : MonoBehaviour
{
    private void Update()
    {
        if (!particleSystem.IsAlive())
        {
            Destroy(this.gameObject);
        }
    }
}
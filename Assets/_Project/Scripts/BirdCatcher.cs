﻿using UnityEngine;
using System.Collections;

public class BirdCatcher : MonoBehaviour
{
    private GameController game;

    void Start ()
    {
        game = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();
    }

    private void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.CompareTag("bird"))
        {
            Debug.Log("caught");
        }
    }

    private void OnTriggerEnter2D(Collider2D col)
    {
        if (col.CompareTag("bird"))
        {
            game.Setup();
        }
    }
}

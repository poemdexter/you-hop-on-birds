/*
Version		Change

0.09.109	Initial Release (xARM Gallery window)


0.09.128
		Added xARM Preview window

		xARM Options:
		 + OS column
		 + Format column


1.00.153
		Added Delegates to run custom code:
		 + OnPreScreenCapUpdate
		 + OnPostScreenCapUpdate
		 + OnFinalizeScreenCapUpdate
		and example how to use:
		 + xARMDelegatesExample.cs
			
		Added iPhone 5C and 5S
		
		Performance improvements
		Several Bugfixes
	
		
1.01.166
		xARM Preview:
		 + 1:1px (pixel perfect) option
		 + 1:1 physical size (device dimensions) option
		 
		Improved interaction with GameView
		Several Bugfixes


1.02.198
		xARM Gallery:
		 + Scale Ratio ('fingerprint')
			
		new ScreenCaps:
		 + iPad Mini 2
		 + iPad Air
		 + Nexus 7 2. generation
		 + Nexus 5
		 
		Android ScreenCaps with system/navigation bar offset (based on density group)
		Bugfixes


1.03.226
		xARM Preview:
		 + Game View inherits xARM Preview resolution option
		 + Fallback Game View resolution option
		
	 	xARM Options:
	 	 + Frames to wait between Game View resize and ScreenCap update option 
	 	
	 	Bugfix: Endless update in Pause mode
	 	Several minor improvements and bugfixes


1.03.233
		Bugfix: xARMSettings.xml encoding error on Korean systems
		Bugfix: GameView positon makes toolbar unreachable


1.04.261
		xARM Preview:
			+ Auto update in Play mode
			+ Export ScreenCap as PNG
			+ Export all ScreenCaps as PNGs
		
		xARM Gallery:
			+ Click on ScreenCap selects it in xARM Preview
			+ Export all ScreenCaps as PNGs
		
		xARM Options:
			+ New Layout for better overview
			> xARM Preview:
				+ Update xARM Preview while Game View has focus option
				+ Update limit in Edit and Play mode options
			> xARM Gallery:
				+ Update limit in Edit mode option
		
		ScreenCaps:
			+ Navigation Bar (Android) ScreenCap filter
			+ ScreenCap infos updated to latest stats (Unity 2014-03)
			+ 3 new Android ScreenCaps
			+ 3 new Standalone ScreenCaps
			+ Windows Phone 8 support (7 ScreenCaps)
			+ Windows RT support (3 ScreenCaps)
			+ Custom ScreenCap section
	
		Bugfix: Improved xARMSettings.xml handling for versioning (Perforce, etc.)
		Bugfix: No more Xcode Build warning "The class defined in script file named 'xARMProxy' does not match the file name!"

1.04.266
	Bugfix: PNG export on Windows creates no longer partial transparent images
	
	
*/
/*
///////////////////////////////////////////
     xARM: Aspect and Resolution Master
          Copyright © 2013-2014
               Flying Whale
             Version 1.04.266
          support@flyingwhale.de
///////////////////////////////////////////

Thanks for purchasing xARM and thereby feeding an indie developer!
I'd appreciate if you support xARM also by rating it in the AssetStore:
https://www.assetstore.unity3d.com/#/content/10563

If you have any problems, questions, feedback or feature requests
please send me an email (support@flyingwhale.de).
Or you can contact me (Thavron) via Unity forum:
http://forum.unity3d.com/threads/196174-xARM-Aspect-and-Resolution-Master

--------------
How to Use
--------------
Place xARM somewhere in your project. Do NOT put it in the Editor folder.
All xARM windows can be found in Unity menu bar under "Window/xARM".

Please keep the Game View undocked while working with xARM.
If the Game View is docked xARM will undock it while updating the ScreenCaps. 
You can put the Game View window in the background and use xARM Preview 
and xARM Gallery to preview your game.
Note: You can bring the Game View to foreground by pressing CTRL/CMD+2.
	
- xARM Preview:
	You can use xARM Preview to focus on one resolution. In contrast 
	to the Game View you can see your game rendered in the selected 
	resolution independent of window's size.
	
	Update "1x", "Editor", "Pause" or "Play"
		- "1x", "Editor", "Pause"
			These options work the same way as described in the "xARM Gallery" 
			section, except they update only the selected ScreenCap.
			
		- "Play"
			Activate this option to update the selected ScreenCap while in 
			Play mode. It gives a live preview.
	
	"1:1px"
		Activate this option to preview the ScreenCap at 1:1px (pixel perfect).
		This is a great help to check for blur, scaling issues etc.
	
	"1:1phy"
		Activate this option to preview the ScreenCap at 1:1 physical size
		(device dimensions). This is a great help to check button sizes etc. 
	
	"Tools"
		- "Export ScreenCap as PNG"
			Exports the selected ScreenCap as PNG file. Target folder and 
			file name can be specified.
		
		- "Export all ScreenCaps as PNGs"
			Exports all active ScreenCaps as PNG files. Target folder can be 
			specified. File names are set automatically.
		
		- "Options"
			Opens xARM Options window.
	
	
- xARM Gallery:
	You can use xARM Gallery to display several resolutions at the 
	same time. Click on a ScreenCap to select it in xARM Preview.
	
	Update "1x" 
		This option is the most flexible way to update all ScreenCaps 
		whenever you like. You can use it in editor, pause mode and 
		even while playing.
	
	Update "Editor" 
		Activate this option to automatically updates all ScreenCaps 
		whenever the Scene changes so that you directly see the results. 
	
	Update "Pause" 
		Activate this option to update all ScreenCaps each time you pause or 
		step the game. You can use Unity's hotkeys to pause 
		(CTRL/CMD+SHIFT+P) and step (CTRL/CMD+ALT+P) the game and 
		instantly update all ScreenCaps.
	
	"Tools"
		- "Export all ScreenCaps as PNGs"
			Exports all active ScreenCaps as PNG files. Target folder can be 
			specified. File names are set automatically.
		
		- "Options"
			Opens xARM Options window.
	
	Note: xARM's ScreenCaps are updated during a couple of frames. 
	Because of that they doesn't display the same frame.
	If needed you can use xARM's Delegates to freeze your game while 
	updating the ScreenCaps to capture the exact same state.
	
	
- xARM Options
	xARM Options holds all xARM settings. Here you can configure xARM
	to fit your needs.
	All Settings are saved under "ProjectSettings/xARMSettings.xml"
	(for 3.x "xARMSettings.xml").
	
	"Global options"
		- "Editor DPI"
			Specify DPI of your display to enable preview at 1:1 physical
			size (xARM Preview) and Scale Ratio (xARM Gallery).
			You can verify the value by measuring the white box if you 
			like to.
		
		- "Export"
			Specify the default folder for PNG export.
		
		- "Update delay"
			This option is used get correct results with image effects,
			other extensions, etc. which need more than one frame to 
			react to resolution changes. Specify how many frames to wait
			between Game View resize and ScreenCap update.
			In most cases 2 frames should be sufficient.
	
	
	"xARM Preview options"
		- "Update limit"
			Specify the maximum updates per second (FPS) for Edit and Play mode.
		
		- "Update while Game View has focus"
			If activated xARM Preview will update even while Game View has focus 
			(deactivate for pre v1.4 behavior).
		
		- "Resize Game View to selected resolution (recommended)"
			If activated selecting a ScreenCap in xARM Preview will also change 
			Game View's resolution. Also after ScreenCap updates xARM returns 
			to the selected resolution.
			If this option is deactivated (or no ScreenCap is selected in 
			xARM Preview) xARM always uses the specified "Fallback Game 
			View resolution" (pre v1.3 behavior).
	
	
	"xARM Gallery options"
		- "Update limit"
			Specify the maximum updates per second (FPS) for Edit mode.
			(All active ScreenCaps are updated as one batch.)
		
		- "ScreenCap size"
			By default size of ScreenCaps in xARM Gallery are determined
			automatically based on available space. 
			If "Fixed" is activated you can set ScreenCap's size in 
			xARM Gallery manually.
		
		- "Scale Ratio"
			Scale Ratio is a reference size in xARM Gallery. By default
			it is the recommended touch target size.
			You can use it to check the button sizes etc.
	
	
	"ScreenCaps"
		Choose Aspect Ratios and Resolutions (ScreenCaps) you are 
		targeting. To make your choice more easy, take a look at the 
		provided information (Stats etc.).
		Sorting the list also affects the ScreenCap order in 
		all xARM windows.
		
		- "Filter"
			Display/hide "Portrait", "Landscape" or "Navigation Bar (Android)" ScreenCaps as needed.
		
		- "iOS", "Android", "Windows Phone 8", "Windows RT" and "Standalone"
			Select the ScreenCaps you'd like to preview.
		
		- "Custom"
			You can add your custom ScreenCaps here.
			(see xARMManager.cs line 298 "How to add custom ScreenCaps").
	
	
- Run your custom code

	You can run your own code (e.g. to recreate your GUI if it 
	doesn't automatically) before or after each ScreenCap is 
	updated or after all ScreenCaps are updated. Use the 
	following Delegates to hook your code:
	 - xARMManager.OnPreScreenCapUpdate
	 - xARMManager.OnPostScreenCapUpdate
	 - xARMManager.OnFinalizeScreenCapUpdate
	(See xARMDelegatesExample.cs for further information)
	

----
FAQ
----
	Q: How do I update xARM?
	A: Follow these steps:
		1. Remove the xARM Folder from your Project
		2. Import the new xARM version from the Asset Store
		3. Close and reopen Unity
	
	
	Q: Why do I get "Cleaning up leaked objects in scene since no 
		game object, component or manager is referencing them ..." 
		in the Console?
	A: Unity sends this message to inform you that some optimizations 
		have been made. You can ignore this.
	
	
	Q: Why do I get the warning "Could not update all ScreenCaps. 
		Switch 'GameView' to 'Free Aspect'.", although it is already set 
		to 'Free Aspect'?
	A: That's caused by maximizing e.g. the GameView. Unfortunately I 
		couldn't find a fix for that yet.
		Workaround: Maximize and de-maximize the Scene View by focusing it
		and pressing Shift+Space (Space for older Unity versions). 
	
------------
Source Code
------------
xARM's goal is to help you to master all issues regarding the mass 
of different aspect and resolution combinations.

Feel free to change xARM's source code to fit your needs, 
however support is limited to the official versions.
Please contact me if you add something that is also useful for 
other xARM users and I will check if it can be included in a 
future release.

-----------
Disclaimer
-----------
xARM can not simulate every aspect of a device or its display
therefore it can not fully replace a test on the physical device.

The statistics (Percent and Positioning) are related to Resolution and 
are based on 'Unity Hardware Statistics 2014-03' (stats.unity3d.com). 
Most other information are based on www.wikipedia.org.

Although all information were compiled with great care accuracy of 
information can not been guaranteed.
All names, brands, trademarks and registered trademarks mentioned in xARM 
are the property of their respective owners. 
They are only used as some representative samples of their type.

*/
